<?php

class DatawrapperPlugin_ExportPrintPdf extends DatawrapperPlugin {

    public function init() {
        $plugin = $this;
        // hook into chart publication
        DatawrapperHooks::register(DatawrapperHooks::GET_CHART_ACTIONS, function() use ($plugin) {
            // no export possible without email
            $cfg = $plugin->getConfig();
            return array(
                'id' => 'export-print-pdf',
                'title' => __("publish / button", $plugin->getName()),
                'icon' => 'print',
                'order' => 350
            );
        });

        // provide static assets files
        $this->declareAssets(
            array('print-pdf.js'),
            "#/chart|map/[^/]+/publish#"
        );

    }

}
